<?php require './examen.php' ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Area de un triangulo</title>
    <style>
        .descripcion {
            font-family: sans-serif;
        }

        .datos {
            font-family: Arial, Helvetica, sans-serif;
        }

        .resultado {
            font-family: 'Courier New', Courier, monospace;
        }
    </style>
</head>

<body>
    <div class="descripcion">
        <h1>Area de un triangulo</h1>
        <p>
            Desarrolle un programa php para imprimir el área 
            de los triángulos cuyos lados <br>
            (cada uno) van desde cero hasta 9. 
            (Se esperan 1000 resultados.) <br>
            La salida es una por renglón. Por ejemplo 
            tres salidas serían: <br> <br>
            Para a=1, b=3, c=2 el área es 2.5 <br>
            Para a=0, b=4, c=0 no es un triángulo. <br>
            Para a=2, b=8, c=1 no es un triángulo. <br>
        </p>
    </div>
    <hr>
    <!-- Ciclo for para mostrar 1000 veces el resultado -->
    <?php for ($h = 0; $h < 1000; $h++) : ?>
        <?php
        // Se generan los numeros aleatorios para los lados de los triangulos
        $a = random_int(0, 9); // lado 1
        $b = random_int(0, 9); // lado 2
        $c = random_int(0, 9); // lado 3

        // Comprueba si es un triangulo, entonces realiza la operación
        if (esTriangulo($a, $b, $c)) {
            $perimetro = calcPerimetro($a, $b, $c);
            // si el resultado de lo anterior no es un numero 
            // entonces los valores no formaban un triangulo
            if (is_nan($perimetro)) {
                $perimetro = "NO ES UN TRIANGULO";
            }
        } else {
            $perimetro = "NO ES UN TRIANGULO";
        }

        ?>
        <div class="datos">
            <h2>Datos</h2>
            <h3>Iteracion <?= $h + 1 ?></h3>
            <p>El lado a es: <?= $a ?> metros</p>
            <br>
            <p>El lado b es: <?= $b ?> metros</p>
            <br>
            <p>El lado c es: <?= $c ?> metros</p>
        </div>
        <br>
        <div class="resultado">
            <h2>Resultado: </h2>
<!-- Se comprueba si la variable no es un string, 
        entonces imprime el resultado -->
            <?php if (!(is_string($perimetro))) : ?>
            <p>Area = <strong><?= $perimetro ?></strong> m<sup>2</sup></p>
<!-- Si sí es un string entonces muestra el mensaje -->
            <?php else : ?>
            <strong>
                <p><?= $perimetro ?></p>
            </strong>
            <?php endif; ?>
            <hr>
        <?php endfor; ?>
        </div>
</body>

</html>