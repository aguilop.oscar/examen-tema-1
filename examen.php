<?php

// Se va a calcular el area con la formula de Heron
// sqrt(p(p-a) (p-b)(p-c))
// Donde p es el semiperimetro
// Y a, b, c son los lados del triangulo
function calcPerimetro($_a, $_b, $_c): float
{
    $p = ($_a + $_b + $_c) / 2;
    return sqrt($p * ($p - $_a) * ($p - $_b) * ($p - $_c));
}

// funcion que comprueba si los datos dados forman un triangulo
function esTriangulo($_a, $_b, $_c): bool
{
    $bool = false;
    if (($_a <= 0 || $_b <= 0 || $_c <= 0)) {
        $bool = false;
    } else {
        $bool = true;
    }
    return $bool;
}
